// Soal No. 1 (Array to Object)
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    var personObj = {}
    if (arr.length != 0){
        for (var i = 0; i < arr.length; i++){
            personObj.firstName = String(arr[i][0])
            personObj.lastName = String(arr[i][1])
            personObj.gender = String(arr[i][2])
            if ((arr[i][3]) && (arr[i][3] < thisYear)) {
                personObj.age = String(thisYear - arr[i][3])
            }
            else{ 
                personObj.age = "Invalid birth year" 
            }
        console.log (i+1,".",personObj.firstName, personObj.lastName, ":", personObj)
        }
    }
    else
        console.log('""')
}

 
// Driver Code
console.log ("NOMOR 1 ARRAY TO OBJECT\n")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var sale = {
        sepatu :  {stacattu : 1500000},
        baju : {"Zoro" : 500000, "H&N" : 250000},
        sweater : {"Uniklooh" : 175000},
        casing : {"Handphone" : 50000}
    }
    var shopperObj = {
        memberId : memberId,
        money : money,
        listPurchased : [],
        changeMoney : 0
    }
    //console.log(Object.keys(sale.baju))
    //console.log(Object.values(sale.baju))
    //console.log(Object.entries(sale.baju))
    if ((memberId)&&(money)&&(money>=50000)){
        //if true
        //console.log (memberId," ",money)
        //shopperObj.memberId = memberId
        //shopperObj.money = money
        //console.log(sale.sepatu.stacattu)
        if (money >= sale.sepatu.stacattu){
            money = money - sale.sepatu.stacattu
            //console.log(money)
            shopperObj.listPurchased.push ("Sepatu Stacattu")
        }
        if (money > sale.baju.Zoro){
            money = money - sale.baju.Zoro
            //console.log(money)
            shopperObj.listPurchased.push ("Baju Zoro")
        }
        if (money >= sale.baju["H&N"]){
            money = money - sale.baju["H&N"]
            //console.log(money)
            shopperObj.listPurchased.push ("Baju H&N")
        }
        if (money >= sale.sweater.Uniklooh){
            money = money - sale.sweater.Uniklooh
            //console.log(money)
            shopperObj.listPurchased.push ("Sweater Uniklooh")
        }
        if (money >= sale.casing.Handphone){
            money = money - sale.casing.Handphone
            //console.log(money)
            shopperObj.listPurchased.push ("Casing Handphoe")
        }
        shopperObj.changeMoney = money 
        return shopperObj
    }
    else if (money<50000){
        return "Mohon Maaf, uang tidak cukup"
    }
    else{
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
  }
   
  // TEST CASES
  console.log("\n NOMOR 2 SHOPPING TIME\n")
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// No. 3 (Naik Angkot)

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //console.log (rute.indexOf('A'))
    //your code here
    //tarif per rute * index array
    var angkotData = []
    //var angkot = {}
    if (arrPenumpang.length != 0){
        for (var i = 0; i < arrPenumpang.length; i++){
            angkot={}
            //console.log(arrPenumpang[i])
            angkot.penumpang = String(arrPenumpang[i][0])
            angkot.naikDari = String(arrPenumpang[i][1])
            angkot.tujuan = String(arrPenumpang[i][2])
            angkot.bayar = (rute.indexOf(angkot.tujuan) - rute.indexOf(angkot.naikDari)) * 2000
            //console.log(angkot[i])
            angkotData.push(angkot)
            //console.log(angkotData)
            //return angkotData
            }
            //angkotData.push(angkot)
        //console.log(angkot)
        //angkotData1.push(angkotData)
        return angkotData
    }
    else{
        return angkotData
    }
  }
   
  //TEST CASE
  console.log("\n NOMOR 3 NAIK ANGKOT\n")
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]