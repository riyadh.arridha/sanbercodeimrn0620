console.log ('No. 1 (Range)')
function range(startNum, finishNum){
    var arr=[]
    if (finishNum < startNum){ //mengecek apakah finishNum lebih besar atau tidak
        for (var i = startNum; i >= finishNum; i--){  // jika lebih besar, maka menggunakan perulangan ini
            arr.push(i)
        }
    }
    else if (finishNum){  //kondisi kedua, apakah finishNum kosong atau tidak, jika tidak kosong dan tidak memenuhi kondisi pertama, berarti normal, kecil ke besar
        for (var i = startNum; i <= finishNum; i++){
            arr.push(i)
        }
    }
    else{ //jika finishNum tidak berisi, alias hanya parameter startNum yang melewatkan data
        return -1
    }
    return arr
}

 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log()
//Batas nomor 1

console.log('No. 2 Range with Step')
function rangeWithStep(startNum, finishNum, step){
    var arr=[]
    if (finishNum < startNum){  //jika parameter kedua lebih besar
        for (var i = startNum; i >= finishNum; i-=step){ //maka  d tampung secara mundur
            arr.push(i)
        }
    }
    else if (finishNum){ //jika finishNum tdk kosong, maka normal, parameter pertama lebih kecil dari parameter kedua
        for (var i = startNum; i <= finishNum; i+=step){
            arr.push(i)
        }
    }
    else{ // jika finihNum kosong, maka kembalikan -1
        return -1
    }
    return arr

}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log()
//Batas nomor 2

console.log('No. 3 Sum of Range')
function sum(startNum, finishNum, step=1){ //berikan step nilai default 1, jika d kosongkan
    var total=0
    if (finishNum < startNum){ 
        for (var i = startNum; i >= finishNum; i-=step){
            total += i 
        }
    }
    else if (finishNum){
        for (var i = startNum; i <= finishNum; i+=step){
            total += i 
        }
    }
    else if (startNum){ //jika hanya startNum yang berisi data, maka kembalikan 1
        return 1
    }
    else{ // jika tidak ada data yang diterima, kembalikan 0
        return 0
    }
    return total

}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log()
//Batas nomor 3

console.log('No. 4 Array Multidimensi')
function dataHandling(n){
    for (var i = 0; i < n.length; i++){
        console.log('Nomor ID: ' + n[i][0] + '\nNama Lengkap: ' + n[i][1] + '\nTTL: ' + n[i][3] + '\nHobi: ' + n[i][4])
        console.log()
    }
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input)
// console.log()
// Batas nomor 4

console.log('No. 5 Balik Kata')
function balikKata(str){
    var revStr = ""
    for (var i = str.length - 1; i >= 0; i--){ //string array nya d panggil dari index paling besar ke kecil dan d simpan ke dalam variabel baru
        revStr += str[i];
    }
    return revStr
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log()
// Batas nomor 5

console.log('No. 6 Metode Array')
function dataHandling2(str){
    str.splice(1,1,"Roman Alamsyah Elsharawy") //splice indeks kesatu, di hapus satu data, dan diganti data baru
    str.splice(2,1,"Provinsi Bandar Lampung") //splice indeks kedua, dihapus satu data, dan diganti dg data baru
    str.splice(4,1,"Pria","SMA Internasional Metro") //splice indeks keempat, hapus satu data (hobby), dan tambahkan dua data baru
    console.log(str)

    date = str[3].split("/") //split tanggal
    var month=""
    switch (date[1]){
        case "01": month = 'Januari';break;
        case "02": month = 'Februari';break;
        case "03": month = 'Maret';break;
        case "04": month = 'April';break;
        case "05": month = 'Mei';break;
        case "06": month = 'Juni';break;
        case "07": month = 'juli';break;
        case "08": month = 'Agustus';break;
        case "09": month = 'September';break;
        case "10": month = 'Oktober';break;
        case "11": month = 'Nopember';break;
        case "12": month = 'Desember';break;
        default: console.log('Bulan tidak diketahui')
    }
    console.log(month)

    dateS = date.slice() 
    dateS.sort(function(value1, value2){return value2 - value1}) //mengurutkan descending data tanggal
    console.log(dateS)

    var dateJ = date.join("-") // join array tanggal dg penghubung "-"
    console.log(dateJ)

    var strS  = str[1].slice(0,15) //memotong string hanya sampai karakter ke 15
    console.log (strS)

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 