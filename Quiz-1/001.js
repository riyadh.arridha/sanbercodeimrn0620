/*

  A. String Terbalik (10 poin)
    Diketahui sebuah function terbalik() yang menerima satu buah parameter berupa tipe data string. Function terbalik() 
    akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. 
    contoh: terbalik("Javascript") akan me-return string "tpircsavaJ", terbalik("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  B. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama maksimum() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 

  C. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 
  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
    
*/

function terbalik(str) {
  // Silakan tulis code kamu di sini
  var revStr = ""
  for (var i = str.length - 1; i >= 0; i--){ //string array nya d panggil dari index paling besar ke kecil dan d simpan ke dalam variabel baru
      revStr += str[i];
  }
  return revStr
}

function maksimum(num1, num2=0) {
  // code di sini
  if ((num1 >= 0) && (num2 >= 0)){
    if (num1 > num2){
      return num1
    }
    else if (num1 == num2){
      return -1
    }
    else if (num1 < num2){
      return num2
    }
    else{
      return -1
    }
  }
  else if ((num1 < 0)||(num2 < 0)){
    return -1
  }
  else {
    return -1
  }
}

function palindrome(str) {
  // Silakan tulis code kamu di sini
  var revStr = ""
  //class = true
  for (var i = str.length - 1; i >= 0; i--){ //string array nya d panggil dari index paling besar ke kecil dan d simpan ke dalam variabel baru
      revStr += str[i];
  }
  if (str == revStr){
    return true
  } 
  else{
    return false
  }
  
}

// TEST CASES String Terbalik
console.log(terbalik("abcde")) // edcba
console.log(terbalik("rusak")) // kasur
console.log(terbalik("racecar")) // racecar
console.log(terbalik("haji")) // ijah

// TEST CASES Bandingkan Angka
console.log(maksimum(10, 15)); // 15
console.log(maksimum(12, 12)); // -1
console.log(maksimum(-1, 10)); // -1 
console.log(maksimum(112, 121));// 121
console.log(maksimum(1)); // 1
console.log(maksimum()); // -1
console.log(maksimum("15", "18")) // 18

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
