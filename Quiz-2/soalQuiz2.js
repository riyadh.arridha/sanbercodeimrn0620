/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor (subject, points, email) {
    this._subject = subject
    this._points = points
    this._email = email
  }
  
  getSubject(){
    return this._subject
  }
  getPoints(){
    return this._points
  }
  getEmail(){
    return this._email
  }
  process () {
    if (this._points.length>1){
      let angka = this._points.reduce ((a,b) => a+b);
      let avg =  angka/this._points.length
      //return {"email" : this._email, "subject" : this._subject, "points" : this._points, "average" : avg}
      return avg
    }
    else{
      //return {"email" : this._email, "subject" : this._subject, "points" : this._points}
      return this._points
    }
  }

  output(){
    return {"email" : this._email, "subject" : this._subject, "points" : this._points, "average" : avg}
    }
}

  angka = new Score("quiz 1", [90,89,88] , "tes@gmail.com")
  console.log("=====NOMOR 1=====")
  console.log ({subject:angka.getSubject(), point:angka.getPoints(), email:angka.getEmail(), average:angka.process()})
  console.log("=================")

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
  //console.log (data)
  //console.log(subject)
  //var score = new Score()
  let tampung=[]
  for (let i = 1; i<data.length; i++){
    for (let j = 0; j<data[1].length; j++){
      //console.log (data[i][j])
      //score("quiz 1", )
      if ((j===1) && (subject=="quiz-1")){
        score = new Score(subject, data[i][j], data[i][0]  )
        tampung.push ({email:score.getEmail(), subject: subject, points: score.getPoints()})
      }
      else if ((j===2) && (subject=="quiz-2")){
        score = new Score(subject, data[i][j], data[i][0]  )
        //console.log (score.process())
        tampung.push ({email:score.getEmail(), subject: subject, points: score.getPoints()})
      }
      else if ((j===3) && (subject=="quiz-3")){
        score = new Score(subject, data[i][j], data[i][0]  )
        //console.log (score.process())
        tampung.push ({email:score.getEmail(), subject: subject, points: score.getPoints()})
      }
    }
  }
  console.log(tampung)
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
   // code kamu di sini
   //nilai = new Score ()
   let tampung = []
   for (let i = 1; i<data.length; i++){
    nilai = new Score("rekap",[data[i][1],data[i][2],data[i][3]], data[i][0])
    //console.log(nilai.process())
    let predikat
    let x = nilai.process()
    if (x > 90){
      predikat = "honour"
    }
    else if (x > 80){
      predikat = "graduate"
    }
    else if (x > 70){
      predikat = "participant"
    }
    output = {email:nilai.getEmail(), average: nilai.process(), predikat: predikat}
    console.log (`${i}.  Email : ${output.email}
    Rata-rata : ${output.average}
    Predikat : ${output.predikat}`)
  }
}
recapScores(data);
