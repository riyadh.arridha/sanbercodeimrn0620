var nama = "Riyadh";
var peran = "Werewolf";
var msg_welcome = "Selamat Datang di Dunia Werewolf";
var msg_name_blank = "Nama harus diisi";
var msg_char_blank = "Pilih peranmu untuk memulai game!";
var msg_penyihir = "kamu dapat melihat siapa yang menjadi werewolf!";
var msg_guard = "kamu akan membantu melindungi temanmu dari serangan werewolf!";
var msg_werewolf = "kamu akan memakan mangsa setiap malam!";

console.log("\n=== IF-ELSE ===")

if (nama == "") {
    console.log(msg_name_blank);
}
else if (peran == ""){
    console.log("Halo "+ nama + ", " + msg_char_blank);
}
else{
    console.log(msg_welcome + ", " + nama);
    if (peran=="Penyihir"){
        console.log("Halo "+ peran + " " + nama + ", " + msg_penyihir );
    }
    else if
    (peran=="Guard"){
        console.log("Halo "+ peran + " " + nama + ", " + msg_guard);
    }
    else if (peran=="Werewolf"){
        console.log("Halo "+ peran + " " + nama + ", " + msg_werewolf );
    }
    else{
        console.log("Unknown Character");
    } 
}
console.log("=== === === ===\n");
console.log("=== SWITCH CASE ===");

var tanggal = 21;
var bulan =1;
var tahun = 1945;
var nm_bulan;
var msg_date_error = "Error! Input tanggal antara 1 - 31.";
var msg_month_error = 'Error! Input bulan antara 1 - 12.';
var msg_year_error = "Error! Input tahun antara 1900 - 2200.";

if (tanggal >= 1  && tanggal <= 31){  // mengecek apakah inputan antara 1 sampai 31
    if (tahun >= 1900 && tahun <= 2200) {  // mengecek apakah inputan 1900 - 2200
        if (bulan >= 1 && bulan <= 12){  // mwengecek apakah inputan 1 - 12
            switch (bulan){
                case 1 : {nm_bulan = "Januari"; break; }
                case 2 : {nm_bulan = "Februari"; break; }
                case 3 : {nm_bulan = "Maret"; break; }
                case 4 : {nm_bulan = "April"; break; }
                case 5 : {nm_bulan = "Mei"; break; }
                case 6 : {nm_bulan = "Juni"; break; }
                case 7 : {nm_bulan = "Juli"; break; }
                case 8 : {nm_bulan = "Agustus"; break; }
                case 9 : {nm_bulan = "September"; break; }
                case 10 : {nm_bulan = "Oktober"; break; }
                case 11 : {nm_bulan = "Nopember"; break; }
                case 12 : {nm_bulan = "Desember"; break; }
                default:  { console.log('Out of range');}
            }
            console.log ("Tanggal yang anda masukkan adalah " + tanggal + " " + nm_bulan +" " + tahun);
        }
        else{
            console.log(msg_month_error);  // pesan yang ditampilkan jika inputan bulan di luar range
        }
    }
    else{
        console.log(msg_year_error); // pesan yang ditampilkan jika inputan tahun di luar range
    }
}
else {
    console.log(msg_date_error); // pesan yang ditampilkan jika inputan tanggal di luar range
}
console.log("=== === === ===\n");





