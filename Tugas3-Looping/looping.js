console.log('\nNo.1 Looping While\n')
console.log('LOOPING PERTAMA')

var angka = 2
while (angka <= 20){
    console.log(angka + " - I Love Coding")
    angka+=2
}
angka-=2

console.log('LOOPING KEDUA')

while (angka >= 2){
    console.log(angka + " - I will become a mobile developer")
    angka-=2
}
console.log("===============================")

console.log('\nNo.2 Looping menggunakan for\n')

for (var i = 1; i<=20; i++){
    if (i%2==1){
        if (i%3==0){
            console.log(i + " - I Love Coding")
        } 
        else {
            console.log(i + " - Santai")
        }
    }
    else { 
        console.log(i + " - Berkualitas")
    }
}
console.log("===============================")

console.log('\nNo.3 Membuat Persegi Panjang #\n')

for (var i = 0; i <= 3; i++){
    for (var j = 0; j <= 7; j++){
        process.stdout.write('#')
    }
    console.log()
}
console.log("===============================")

console.log('\nNo.4 Membuat Tangga\n')

for (var i = 0; i <= 6; i++){
    for (var j = 0; j <= i; j++){
        process.stdout.write('#')
    }
    console.log()
}
console.log("===============================")

console.log('\nNo.5 Membuat Papan Catur\n')

var hitam = '#'
var putih = ' '
for (var i = 0; i <= 7; i++){
    for (var j = 0; j <= 3; j++){
        if (i%2==0){
            process.stdout.write(putih + hitam)
        } 
        else{
            process.stdout.write(hitam + putih)
        }
    }
    console.log()
}
console.log("===============================")