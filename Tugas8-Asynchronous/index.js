// di index.js

var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

//Tulis kode untuk memanggil function readBooks di sini
// readBooks (10000, books[0], callback => {
//     readBooks (callback, books[1], callback => {
//         readBooks (callback, books[2], callback =>{
//             console.log(callback)
//         })
// })
// })
const letStart = (index, times) => {
    index > books.length-1?'':
    readBooks(times, books[index], times => {
        return letStart(index + 1, times)
    })
}
letStart (0,5000)