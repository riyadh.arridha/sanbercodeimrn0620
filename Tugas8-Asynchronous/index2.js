var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
// readBooksPromise(10000, books[0])
//     .then (time => {
//         return readBooksPromise(time, books[1])
//         })
//     .then (time => {
//         return readBooksPromise(time, books[2])
//     })
//     .catch (error =>{
//         console.log(error)
//     })
const letStart = (index, times) => {
    index < books.length-1 ? 
    readBooksPromise(times, books[index]) 
    .then (times => {
        return letStart(index + 1, times)
    })
    :
    readBooksPromise(times, books[index]) 
    .catch (error =>{
        console.log(error)
    }) 
}
letStart (0,7000)