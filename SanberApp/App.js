import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Component1 from "./Latihan/Latihan12Component/App";
import Component from "./Tugas/Tugas12Component/App";
import FlexBox from "./Latihan/Latihan13FlexBox";
import TugasFlexBox from "./Tugas/Tugas13FlexBox/App";
import APILifeCycle from "./Latihan/Latihan14APILifeCycle/App";
import LifeCycle from "./Tugas/Tugas14LifeCycle/App";
import ReactNavigation from "./Tugas/Tugas15ReactNavigation/App";
import Quiz3 from "./Tugas/Quiz-3/";

const App = () => {
  //const App = () => {
  return (
    // <Component1 />
    // <FlexBox />
    // <TugasFlexBox />
    // <APILifeCycle />
    // <LifeCycle />
    // <ReactNavigation />
    <Quiz3 />
  );
};
export default App;
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     // backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
