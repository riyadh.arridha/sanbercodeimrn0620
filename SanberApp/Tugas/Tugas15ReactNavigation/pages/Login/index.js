import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Login } from "./../../assets";
import { Back } from "./../../assets";
import { Button } from "../../components";

const LoginApp = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <View style={styles.buttonBack}>
          <Image source={Back} />
        </View>
      </TouchableOpacity>
      <View style={styles.imageSet}>
        <Image source={Login} />
        <Text style={styles.loginText}>
          Silahkan masukkan data yang dibutuhkan untuk masuk
        </Text>
      </View>
      <InputLogin title="Email" />
      <InputLogin title="Password" />
      {/* <TouchableOpacity>
        <View style={{ alignItems: "center" }}>
          <View style={styles.button}>
            <Text style={styles.loginTitle}>MASUK</Text>
          </View>
        </View>
      </TouchableOpacity> */}
      <View style={{ alignItems: "center", marginTop: 100 }}>
        <Button title="MASUK" onPress={() => navigation.navigate("Skills")} />
      </View>
    </View>
  );
};

const InputLogin = ({ title }) => {
  return (
    <View style={styles.inputBar}>
      <TextInput style={styles.input} placeholder={title} />
    </View>
  );
};

export default LoginApp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  buttonBack: {
    marginTop: 20,
    marginLeft: 15,
  },
  imageSet: {
    alignItems: "center",
    marginTop: 20,
  },
  loginText: {
    marginTop: 36,
    color: "#B53471",
    marginLeft: 64,
    marginRight: 64,
    fontSize: 14,
    textAlign: "center",
    fontWeight: "bold",
    marginBottom: 40,
  },
  input: {
    height: 40,
    borderColor: "#C4C4C4",
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: "#C4C4C4",
    marginBottom: 20,
    fontSize: 12,
  },
  inputBar: {
    marginLeft: 64,
    marginRight: 64,
  },
  loginTitle: {
    color: "#000000",
    textAlign: "center",
    color: "#FFFFFF",
    fontSize: 12,
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "#B53471",
    height: 48,
    width: 232,
    borderRadius: 232 / 2,
    alignItems: "center",
    justifyContent: "center",
  },
});
