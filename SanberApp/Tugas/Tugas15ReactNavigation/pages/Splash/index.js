import React, { useEffect, Component } from "react";
import {
  FlatList,
  View,
  Image,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

const Splash = ({ navigation }) => {
  //ini adalah props bawaan dari navigation container
  useEffect(() => {
    //sama dengan ComponentDidMount di class component
    setTimeout(() => {
      navigation.replace("Welcome"); //.navigate 'back' berfungsi
    }, 2000);
  });
  return (
    <View>
      <Text style={{ textAlign: "center", marginTop: 20 }}>
        Ini adalah Splash
      </Text>
    </View>
  );
};

export default Splash;
