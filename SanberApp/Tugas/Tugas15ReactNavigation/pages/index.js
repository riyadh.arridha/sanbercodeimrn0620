import About from "./About";
import Login from "./Login";
import Register from "./Register";
import Skills from "./Skills";
import Splash from "./Splash";
import Welcome from "./Welcome";
import Project from "./Project";
import Add from "./Add";

export { About, Login, Register, Skills, Splash, Welcome, Project, Add };
