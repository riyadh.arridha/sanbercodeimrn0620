import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { Welcome } from "./../../assets";
import { Button } from "../../components";
import { colors } from "../../utils/colors";

const WelcomePage = ({ navigation }) => {
  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };
  return (
    <View style={styles.container}>
      <View style={styles.image}>
        <Image source={Welcome} />
      </View>
      <View>
        <Text style={styles.welcomeText}>Selamat Datang di SkilPro</Text>
      </View>
      <ButtonApp
        desc="Silahkan pilih masuk, jika anda sudah memiliki akun"
        title="MASUK"
        onPress={() => handleGoTo("Login")}
      />
      <ButtonApp
        desc="Atau pilih daftar, jika anda belum memiliki akun"
        title="DAFTAR"
        onPress={() => handleGoTo("Register")}
      />
    </View>
  );
};

const ButtonApp = ({ desc, title, onPress }) => {
  return (
    <View style={{ alignItems: "center" }}>
      <View>
        <Text style={styles.loginDesc}>{desc}</Text>
      </View>

      <Button title={title} onPress={onPress} />
      {/* <TouchableOpacity>
        <View style={styles.button}>
          <Text style={styles.loginTitle}>{title}</Text>
        </View>
      </TouchableOpacity> */}
    </View>
  );
};

export default WelcomePage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#ffffff',
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: 186,
    height: 139,
  },
  welcomeText: {
    fontSize: 24,
    color: colors.default,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 11,
    marginBottom: 50,
  },
  loginDesc: {
    fontSize: 12,
    color: "#BFBFBF",
    textAlign: "center",
    paddingHorizontal: 60,
  },
});
