import React from "react";
import {
  View,
  Image,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

import { About, Login, Register, Skills, Splash, Welcome } from "./pages";
import Router from "./router";
import { NavigationContainer } from "@react-navigation/native";

export default class App extends React.Component {
  render() {
    return (
      // return <Skills />;
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    );
  }
}
