import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import {
  Splash,
  Login,
  Welcome,
  Register,
  Skills,
  About,
  Projects,
  Add,
} from "../pages";
import { Header } from "react-native/Libraries/NewAppScreen";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Welcome"
        component={Welcome}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      {/* <Stack.Screen
        name="Skills"
        component={Skills}
        options={{ headerShown: false }}
      /> */}
      <Stack.Screen
        name="About"
        component={About}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
    <Tab.Navigator>
        <Tab.Screen name="Home" component={Skills} />
        <Tab.Screen name="Settings" component={Projects} />
      </Tab.Navigator>

  );
  }


export default Router;
