import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image, 
    TouchableOpacity
} from 'react-native';
import welcomePict from './welcome.jpg'

const Welcome = ()=>{
    return(
    <View style={styles.container}>
            <View style={styles.image}>
                <Image source={welcomePict}/>
            </View>
            <View>
                <Text style={styles.welcomeText}>Selamat Datang di SkilPro</Text>
            </View>
            <ButtonApp desc='Silahkan pilih masuk, jika anda sudah memiliki akun' title='MASUK'/>
            <ButtonApp desc='Atau pilih daftar, jika anda belum memiliki akun' title='DAFTAR'/>
        </View>
    )
}

const ButtonApp = ({desc,title})=> {
    return(
        <View style={{alignItems:'center'}}>
            <View>
                <Text style={styles.loginDesc}>{desc}</Text>
            </View>
            <TouchableOpacity>
                <View style={styles.button}>
                    <Text style={styles.loginTitle}>{title}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default Welcome;

const styles = StyleSheet.create({
    container: {
      flex: 1,
    //   backgroundColor: '#ffffff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    image: {
        width:186, 
        height:139, 
    },
    welcomeText: {
        fontSize:24,
        color:'#B53471',
        fontWeight:'bold',
        textAlign:'center',
        marginTop:11,
        marginBottom:50
    },
    loginDesc:{
        fontSize:12,
        color:'#BFBFBF',
        textAlign:'center',
        paddingHorizontal:60
    },
    loginTitle:{
        fontSize:12,
        // color:'#FFFFFF'
        color:'#000000',
        textAlign:'center'
    },
    button: {
        backgroundColor:'#B53471',
        height:48,
        width:232,
        marginTop:11,
        marginBottom:11,
        justifyContent:'center',
        borderRadius:232/2
    }
  });