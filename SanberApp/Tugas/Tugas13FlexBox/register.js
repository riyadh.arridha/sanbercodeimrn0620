import React, {useState} from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image, 
    TextInput,
} from 'react-native';
import registerPict from './register.jpg'
import back from './back.jpg'

const Register = ()=>{
    return(
        <View style={styles.container}>
            <TouchableOpacity>
            <View style={styles.buttonBack}>
                <Image source={back}/>
            </View>
            </TouchableOpacity>
            <View style={styles.imageSet}>
                <Image source={registerPict}/>
                <Text style={styles.registerText}>Silahkan lengkapi data anda untuk daftar</Text>
            </View>
            <InputData title='Nama Lengkap'/>
            <InputData title="Email"/>
            <InputData title="Password"/>
            <InputData title="Konfirmasi Password"/>
            <TouchableOpacity>
            <View style={{alignItems:'center'}}>
                <View style={styles.button}>
                    <Text style={styles.registerTitle}>DAFTAR</Text>
                </View>
            </View>
            </TouchableOpacity>
        </View>
    );
}

const InputData = (title)=> {
    const [text, setText] = useState('');
    const desc = title.title;
    console.log(desc)
  return (
    //<View style={{padding: 2, backgroundColor:'yellow'}}>
    <View style={styles.inputBar}>
      <TextInput
        style={styles.input}
        placeholder={desc}
        onChangeText={text => setText(text)}
        defaultValue={text}
      />
    </View>
    )
}

export default Register;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
    },
    buttonBack:{
        marginTop:20,
        marginLeft:15
    },
    imageSet:{
        alignItems:'center'
    },
    registerText:{
        marginTop:36,
        color:'#B53471',
        marginLeft:64,
        marginRight:64,
        fontSize:14,
        textAlign:'center',
        fontWeight:'bold',
        marginBottom:40,
    },
    input:{
        height: 40, 
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        borderTopWidth:0,
        borderRightWidth:0,
        borderLeftWidth:0,
        color:'#C4C4C4',
        marginBottom:20,
        fontSize:12,
    },
    inputBar:{
        marginLeft:64,
        marginRight:64
    },
    registerTitle:{
        color:'#000000',
        textAlign:'center',
        color:'#FFFFFF',
        fontSize:12,
        fontWeight:'bold'
    },
    button: {
        backgroundColor:'#B53471',
        height:48,
        width:232,
        marginTop:15,
        borderRadius:232/2,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
