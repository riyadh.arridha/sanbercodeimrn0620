import React, {useState} from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity, 
} from 'react-native';
import Photo from './photo.png'
import Facebook from './facebook.png'
import Instagram from './instagram.png'
import Gitlab from './gitlab.png'
import Twitter from './twitter.png'
import Back from './back.jpg'

const About = ()=> {
    return(
        <View style={styles.container}>
            <TouchableOpacity>
            <View style={styles.header}>
                    <Image source={Back}></Image>
            </View>
            </TouchableOpacity>
            <View style={styles.atas}>
                <Image source={Photo}></Image>
                <Text style={styles.caption}>Riyadh Arridha</Text>
            </View>
            <View style={styles.tengah}>
                <Image source={Facebook}></Image>
                <Text style={styles.fontTengah}>@riyadh.arridha</Text>
                <Image source={Instagram}></Image>
                <Text style={styles.fontTengah}>@riyadh.arridha</Text>
                <Image source={Twitter}></Image>
                <Text style={styles.fontTengah}>@riyadh_arridha</Text>
                <Text style={styles.caption}>Social Media Account</Text>
            </View>
            <View style={styles.bawah}>
                <Image source={Gitlab}></Image>
                <Text style={styles.fontBawah}>gitlab/riyadh.arridha/</Text>
                <Text style={styles.caption}>My Awesome Project</Text>
            </View>
        </View>
    )
}

export default About;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
      justifyContent:'space-evenly',
    },
    header:{
        height:'5%',
        marginTop:35,
        // backgroundColor:'yellow',
        marginLeft:15,
    },
    atas:{
        height:'30%', 
        justifyContent:'center',
        alignItems:'center',
        // backgroundColor:'red',
    },
    tengah:{
        justifyContent:'center',
        alignItems:'center',
        height:'40%',
        // backgroundColor:'blue'
    },
    fontTengah:{
        fontSize:14,
        fontWeight:'bold',
        color:'#BFBFBF',
        margin:7,
    },
    caption:{
        fontSize:18,
        color:'#B53471',
        fontWeight:'bold',
        margin: 15
    },
    bawah:{
        justifyContent:'center',
        alignItems:'center',
        height:'25%',
        // backgroundColor:'green'
    },
    fontBawah:{
        fontSize:14,
        fontWeight:'bold',
        color:'#BFBFBF',
        margin:7,
    },
})