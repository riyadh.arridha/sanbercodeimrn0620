import React, {useState} from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image, 
    TextInput,
    TouchableOpacity,
} from 'react-native';
import loginPict from './login.png'
import back from './back.jpg'

const Login = ()=>{
    return(
        <View style={styles.container}>
            <TouchableOpacity>
            <View style={styles.buttonBack}>
                <Image source={back}/>
            </View>
            </TouchableOpacity>
            <View style={styles.imageSet}>
                <Image source={loginPict}/>
                <Text style={styles.loginText}>Silahkan masukkan data yang dibutuhkan untuk masuk</Text>
            </View>
            <InputLogin title="Email"/>
            <InputLogin title="Password"/>
            <TouchableOpacity>
            <View style={{alignItems:'center'}}>
                <View style={styles.button}>
                    <Text style={styles.loginTitle}>MASUK</Text>
                </View>
            </View>         
            </TouchableOpacity>
        </View>
    );
}

const InputLogin = (title)=> {
    const [text, setText] = useState('');
    const desc = title.title;
    console.log(desc)
  return (
    //<View style={{padding: 2, backgroundColor:'yellow'}}>
    <View style={styles.inputBar}>
      <TextInput
        style={styles.input}
        placeholder={desc}
        onChangeText={text => setText(text)}
        defaultValue={text}
      />
    </View>
    )
}

export default Login;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
    },
    buttonBack:{
        marginTop:20,
        marginLeft:15
    },
    imageSet:{
        alignItems:'center'
    },
    loginText:{
        marginTop:36,
        color:'#B53471',
        marginLeft:64,
        marginRight:64,
        fontSize:14,
        textAlign:'center',
        fontWeight:'bold',
        marginBottom:40,
    },
    input:{
        height: 40, 
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        borderTopWidth:0,
        borderRightWidth:0,
        borderLeftWidth:0,
        color:'#C4C4C4',
        marginBottom:20,
        fontSize:12,
    },
    inputBar:{
        marginLeft:64,
        marginRight:64
    },
    loginTitle:{
        color:'#000000',
        textAlign:'center',
        color:'#FFFFFF',
        fontSize:12,
        fontWeight:'bold'
    },
    button: {
        backgroundColor:'#B53471',
        height:48,
        width:232,
        marginTop:110,
        borderRadius:232/2,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
