import React, {Component} from 'react';
import { FlatList, View, Image, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import AppImage from './assets/welcome.jpg'
import skillData from './skillData.json'
import SkillItem from './skillItem'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class App extends React.Component {

    render(){
        // alert(skillData.items[0])
        return(
            <View style={styles.container}>
                
                <View style={styles.profilView}>
                    <View style={styles.logoView}  >
                        <Image source={AppImage} style={styles.logoImage}></Image>
                        <Text style={styles.fortofolioText}>FORTOFOLIO</Text>
                    </View>
                    <View style={styles.accountView}>
                        <Icon name="account-circle" color='#B53471'  size={30}/>
                        <View>
                            <Text style={styles.nameText}>Hai,</Text>
                            <Text style={styles.nameText}>Riyadh Arridha</Text>
                        </View>  
                    </View>
                    <Text style={styles.skillText}>SKILL</Text>
                    <View style={styles.borderView}>
                    </View>
                </View>

                <View style={styles.captionView}>
                    <View style={styles.captionListView}>
                    <Text style={styles.captionText}>Library/Framework</Text>
                    </View>
                    <View style={styles.captionListView}>
                    <Text style={styles.captionText}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.captionListView}>
                    <Text style={styles.captionText}>Teknologi</Text>
                    </View>
                </View>

                {/* <ScrollView> */}
                <View style={styles.bodyView}>
                   {/* <SkillItem skill={skillData.items[0]}/>
                   <SkillItem skill={skillData.items[1]}/>
                   <SkillItem skill={skillData.items[2]}/>
                   <SkillItem skill={skillData.items[3]}/>
                   <SkillItem skill={skillData.items[4]}/>
                   <SkillItem skill={skillData.items[5]}/> */}
                   <FlatList
                   data = {skillData.items}
                   renderItem={(skill)=><SkillItem skill={skill.item} />}
                   keyExtractor={item=>item.id}
                   />
                 </View>
                {/* // </ScrollView> */}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      backgroundColor:'white',
      flex:1,
      marginLeft:10,
      marginRight:10,
      marginTop:10,
      marginBottom:10
    },
    profilView:{
        height: '20%',
        // backgroundColor:'yellow',
    },
    logoView:{
        position:'absolute', 
        right:0,
    },
    logoImage:{
        maxHeight:135,
        maxWidth:180,
    },
    fortofolioText:{
        color:'#B53471', 
        fontSize:12,
        position:'absolute', 
        left:17, 
        bottom:7,
        fontWeight:'bold'
    },
    accountView:{
        marginTop:35,
        flexDirection:'row',
        alignItems:'center',
        marginLeft:5,
    },
    nameText:{
        fontSize: 16, 
        color:'#B53471',
        marginLeft : 5
    },
    skillText:{
        fontSize:36, 
        color:'#B53471', 
        marginLeft:5,
        marginTop:5,
    },
    borderView:{
        height:4, 
        backgroundColor:'#B67891',
    },
    captionView:{
        height: '7%',
        // backgroundColor:'green',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center'
    },
    captionListView:{
        backgroundColor:'#ECCCDB',
        padding:5, 
        borderRadius:5, 
        margin:3,
    },
    captionText:{
        color:'#B53471',
        fontWeight:'bold',
        fontSize:14,
    },
    bodyView:{
        flex:1,
        // backgroundColor:'blue',
        alignItems:'center',
    },
   
})