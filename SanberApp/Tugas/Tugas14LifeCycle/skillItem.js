import React, {Component} from 'react';
import { View, Image, FlatList, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import skillData from './skillData.json'
import IconF from 'react-native-vector-icons/Fontisto'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class SkillItem extends Component {
    render(){    
        let skill = this.props.skill;
        // alert(skill.id);
        return (
             <View style={styles.listView} >
             <Icon name={skill.iconName} size={60} color='#B53471'/>
             <View>
                <Text style={styles.skillNameText}>{skill.skillName}</Text>
                 <Text style={styles.skillCategoryText}>{skill.categoryName}</Text>
                 <Text style={styles.percentText}>{skill.percentageProgress}</Text>
             </View>
             <Icon name='chevron-right' size={90} color='#B53471'/>
             </View>
        )
    }
}

const styles = StyleSheet.create({
    listView:{
        backgroundColor:'#ECCCDB',
        height:130,
        width:'100%',
        borderRadius:10,
        alignItems:'center',
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'space-around',
        marginBottom:5,
    },
    skillNameText:{
        fontSize:22,
        fontWeight:'bold',
        color:'#B53471'
    },
    skillCategoryText:{
        fontSize:14,
        fontWeight:'bold',
        color:'#B67891'
    },
    percentText:{
        fontSize:42,
        fontWeight:'bold',
        color:'#FFF',
        textAlign:'right'
    }
})